#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-04 16:36-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 3.6.0\n"

#: casestudies.html%2Bhtml[lang]:4-1
msgid "en"
msgstr ""

#: casestudies.html%2Bhtml.body.nav.div.div.ul.li:88-17
msgid "About"
msgstr ""

#.  TODO: html2po cannot make the href translatable, so point to the Manual's language chooser. 
#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: casestudies.html%2Bhtml.body.nav.div.div.ul.li:91-17
msgid "Support"
msgstr ""

#: casestudies.html%2Bhtml.body.nav.div.div.ul.li:96-17
msgid "News"
msgstr ""

#: casestudies.html%2Bhtml.body.nav.div.div.ul.li:99-17
msgid "Community"
msgstr ""

#: casestudies.html%2Bhtml.body.nav.div.div.ul.li:102-17
msgid "Donate"
msgstr ""

#: casestudies.html%2Bhtml.body.nav.div.div.div.a[title]:106-15
msgid "Get CENO"
msgstr ""

#: casestudies.html%2Bhtml.body.nav.div.div.div:106-10
msgid "Download"
msgstr ""

#.  TRANSLATORS: Replace with your uppercase language code (e.g. "MY" for Burmese). 
#: casestudies.html%2Bhtml.body.nav.div.div.div:108-1
msgid "EN"
msgstr ""

#.  Sorted alphabetically by language code. 
#.  TRANSLATORS: Please do not translate any of these. 
#: casestudies.html%2Bhtml.body.nav.div.div.div.div:114-5
msgid ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/casestudies"
".html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
"href=\"../es/casestudies.html\">Español</a> <a class=\"navbar-item\" lang="
"\"fa\" hreflang=\"fa\" href=\"../fa/casestudies.html\">فارسی</a> <a class"
"=\"navbar-item\" lang=\"fr\" hreflang=\"fr\" href=\"../fr/casestudies.html\""
">Français</a> <a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../"
"my/casestudies.html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" "
"hreflang=\"ru\" href=\"../ru/casestudies.html\">Pусский</a> <a class"
"=\"navbar-item\" lang=\"tr\" hreflang=\"tr\" href=\"../tr/casestudies.html\""
">Türkçe</a> <a class=\"navbar-item\" lang=\"uk\" hreflang=\"uk\" href=\"../"
"uk/casestudies.html\">Українська</a> <a class=\"navbar-item\" lang=\"ur\" "
"hreflang=\"ur\" href=\"../ur/casestudies.html\">اردو</a>"
msgstr ""

#: casestudies.html%2Bhtml.body.main.title:135-1
msgctxt "casestudies.html+html.body.main.title:135-1"
msgid "CENO Browser | Case Studies"
msgstr ""

#: casestudies.html%2Bhtml.body.main.title:136-1
msgctxt "casestudies.html+html.body.main.title:136-1"
msgid "CENO Browser | Case Studies"
msgstr ""

#: casestudies.html%2Bhtml.body.main.div.h1:138-5
msgid "Case Studies"
msgstr ""

#: casestudies.html%2Bhtml.body.main.div.p:139-2
msgid "Sharing the stories of CENO users from around the world."
msgstr ""

#: casestudies.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:150-17
msgid "Highlights from the Ground in Iran"
msgstr ""

#: casestudies.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:152-17
msgid "December 15, 2022"
msgstr ""

#: casestudies.html%2Bhtml.body.footer.div.p:176-9
msgid ""
"CENO Browser is a project by <a href=\"https://equalit.ie\">eQualit.ie</a> – "
"a not-for-profit Canadian company developing open and reusable systems with "
"a focus on privacy, online security and freedom of association. Technology "
"solutions and innovations are driven by our <a href=\"https://equalit.ie/en/"
"values/\">values</a> that guide us to protect the rights and aspirations of "
"everyone online."
msgstr ""

#.  TRANSLATORS: Please do not translate this. 
#: casestudies.html%2Bhtml.body.footer:178-23
msgid ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document.write(\""
"-\"+new Date().getFullYear());</script>"
msgstr ""
