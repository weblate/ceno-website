#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-04 16:36-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 3.6.0\n"

#: help.html%2Bhtml[lang]:4-1
msgid "en"
msgstr ""

#: help.html%2Bhtml.body.nav.div.div.ul.li:88-17
msgid "About"
msgstr ""

#.  TODO: html2po cannot make the href translatable, so point to the Manual's language chooser. 
#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: help.html%2Bhtml.body.nav.div.div.ul.li:91-17
msgid "Support"
msgstr ""

#: help.html%2Bhtml.body.nav.div.div.ul.li:96-17
msgid "News"
msgstr ""

#: help.html%2Bhtml.body.nav.div.div.ul.li:99-17
msgid "Community"
msgstr ""

#: help.html%2Bhtml.body.nav.div.div.ul.li:102-17
msgid "Donate"
msgstr ""

#: help.html%2Bhtml.body.nav.div.div.div.a[title]:106-15
msgid "Get CENO"
msgstr ""

#: help.html%2Bhtml.body.nav.div.div.div:106-10
msgid "Download"
msgstr ""

#.  TRANSLATORS: Replace with your uppercase language code (e.g. "MY" for Burmese). 
#: help.html%2Bhtml.body.nav.div.div.div:108-1
msgid "EN"
msgstr ""

#.  Sorted alphabetically by language code. 
#.  TRANSLATORS: Please do not translate any of these. 
#: help.html%2Bhtml.body.nav.div.div.div.div:114-5
msgid ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/help.html\""
">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" href=\"../"
"es/help.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" hreflang=\"fa"
"\" href=\"../fa/help.html\">فارسی</a> <a class=\"navbar-item\" lang=\"fr\" "
"hreflang=\"fr\" href=\"../fr/help.html\">Français</a> <a class=\"navbar-"
"item\" lang=\"my\" hreflang=\"my\" href=\"../my/help.html\">မြန်မာစာ</a> <a "
"class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../ru/help.html\""
">Pусский</a> <a class=\"navbar-item\" lang=\"tr\" hreflang=\"tr\" href=\"../"
"tr/help.html\">Türkçe</a> <a class=\"navbar-item\" lang=\"uk\" hreflang=\"uk"
"\" href=\"../uk/help.html\">Українська</a> <a class=\"navbar-item\" lang=\"ur"
"\" hreflang=\"ur\" href=\"../ur/help.html\">اردو</a>"
msgstr ""

#: help.html%2Bhtml.body.main.title:135-1
msgctxt "help.html+html.body.main.title:135-1"
msgid "CENO Browser | Community"
msgstr ""

#: help.html%2Bhtml.body.main.title:136-1
msgctxt "help.html+html.body.main.title:136-1"
msgid "CENO Browser | Community"
msgstr ""

#: help.html%2Bhtml.body.main.div.h1:138-5
msgid "Get Involved!"
msgstr ""

#: help.html%2Bhtml.body.main.div.p:139-2
msgid ""
"We'd love your support in making CENO Browser as useful and accessible as "
"possible. CENO is a completely Free/Libre/Open Source project, and there are "
"many ways to contribute!"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:149-17
msgid "Translations"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:151-9
msgid ""
"Help translate the CENO Browser app, web extension, user manual and website! "
"We use Weblate to crowdsource translations."
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:152-13
msgid ""
"Please contact the CENO team at cenoers [at] equalit [dot] ie (or the "
"contact form below) if you're interested in becoming a reviewer for "
"translation(s) in your language(s)."
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:153-13
msgid ""
"For more information about how to use Weblate, see <a href=\"https://docs."
"weblate.org/\">the Weblate documentation.</a>"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:154-17
msgid "Join our Weblate"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:165-17
msgid "User Research"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:166-3
msgid ""
"CENO relies on user feedback to understand the needs of our users and to "
"suggest improvements and new features."
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:167-3
msgid ""
"Sharing your story also helps spread the word about CENO, which grows the "
"network. With your consent, we may also highlight your story on our website!"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:168-3
msgid ""
"We welcome reviews on the Play Store as well as direct communications - you "
"can get in touch with the team via the feedback button at the bottom of this "
"website, or send us an email:"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:169-3
msgid "cenoers [at] equalit [dot] ie"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:170-17
msgid "Take a survey"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:181-17
msgid "Source Code"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:182-3
msgid ""
"If you are interested in CENO's source code please check the following Git "
"repositories:"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p.ul.li:184-12
msgid ""
"Browser components: <a href=\"https://github.com/censorship-no/ceno-browser\""
">package builder</a>, <a href=\"https://github.com/censorship-no/gecko-dev/"
"tree/ceno\">Firefox fork</a>, <a href=\"https:// ithub.com/censorship-no/"
"ceno-distribution\">included extensions</a>, <a href=\"https://github.com/"
"censorship-no/ceno-ext-settings\">settings extension source</a>"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p.ul.li:186-12
msgid "Ouinet source"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:187-12
msgid ""
"You may also be interested in the (no longer maintained) <a href=\"https"
"://github.com/censorship-no-archive/ceno1\">previous incarnation of CENO</"
"a>, built on the <a href=\"https://freenetproject.org/\">Freenet</a> "
"anonymous file sharing and content publishing network. Other inactive "
"project-related repositories can be found at the <a href=\"https://github."
"com/censorship-no-archive\">Censorship.no! archive</a>."
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:188-17
msgid "Browse our Github"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.h2:201-1
msgid "Run a Bridge Node"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:207-3
msgid "Become a bridge!"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:209-17
msgid "Want to help grow the network? Join the swarm!"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:216-3
msgid "The easy way: Android"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:218-17
msgid ""
"Run CENO Browser on your Android device, and/or a spare, always-on device."
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:225-3
msgid "Run the CENO Docker Client"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:227-17
msgid ""
"Alternatively, with just one command you can set up a bridge on your "
"computer or VPS: <code>sudo docker run --name ceno-client -dv ceno:/var/opt/"
"ouinet --network host --restart unless-stopped equalitie/ceno-client</code>"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:234-3
msgid "Open a portal to the internet"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:236-17
msgid "Ensure UPnP is enabled on your router, or set up port forwarding."
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:243-3
msgid "To see if it's working:"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:245-17
msgid ""
"Create and open a new Firefox Profile, and set the proxy to localhost, port "
"8077."
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:252-3
msgid "Thank you for growing the network!"
msgstr ""

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:254-17
msgid ""
"Install the CENO Web Extension and the provided certificate to test it out!"
msgstr ""

#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: help.html%2Bhtml.body.main.div.section.div.div:260-1
msgid "Learn more about setting up a bridge in the User Manual!"
msgstr ""

#: help.html%2Bhtml.body.footer.div.p:281-9
msgid ""
"CENO Browser is a project by <a href=\"https://equalit.ie\">eQualit.ie</a> – "
"a not-for-profit Canadian company developing open and reusable systems with "
"a focus on privacy, online security and freedom of association. Technology "
"solutions and innovations are driven by our <a href=\"https://equalit.ie/en/"
"values/\">values</a> that guide us to protect the rights and aspirations of "
"everyone online."
msgstr ""

#.  TRANSLATORS: Please do not translate this. 
#: help.html%2Bhtml.body.footer:283-23
msgid ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document.write(\""
"-\"+new Date().getFullYear());</script>"
msgstr ""
