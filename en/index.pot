#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-04 16:36-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 3.6.0\n"

#: index.html%2Bhtml[lang]:4-1
msgid "en"
msgstr ""

#: index.html%2Bhtml.body.nav.div.div.ul.li:88-17
msgid "About"
msgstr ""

#.  TODO: html2po cannot make the href translatable, so point to the Manual's language chooser. 
#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: index.html%2Bhtml.body.nav.div.div.ul.li:91-17
msgid "Support"
msgstr ""

#: index.html%2Bhtml.body.nav.div.div.ul.li:96-17
msgid "News"
msgstr ""

#: index.html%2Bhtml.body.nav.div.div.ul.li:99-17
msgid "Community"
msgstr ""

#: index.html%2Bhtml.body.nav.div.div.ul.li:102-17
msgid "Donate"
msgstr ""

#: index.html%2Bhtml.body.nav.div.div.div.a[title]:106-15
msgid "Get CENO"
msgstr ""

#: index.html%2Bhtml.body.nav.div.div.div:106-10
msgid "Download"
msgstr ""

#.  TRANSLATORS: Replace with your uppercase language code (e.g. "MY" for Burmese). 
#: index.html%2Bhtml.body.nav.div.div.div:108-1
msgid "EN"
msgstr ""

#.  Sorted alphabetically by language code. 
#.  TRANSLATORS: Please do not translate any of these. 
#: index.html%2Bhtml.body.nav.div.div.div.div:114-5
msgid ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/index."
"html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" href="
"\"../es/index.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
"hreflang=\"fa\" href=\"../fa/index.html\">فارسی</a> <a class=\"navbar-item\" "
"lang=\"fr\" hreflang=\"fr\" href=\"../fr/index.html\">Français</a> <a class"
"=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/index.html\""
">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../"
"ru/index.html\">Pусский</a> <a class=\"navbar-item\" lang=\"tr\" hreflang="
"\"tr\" href=\"../tr/index.html\">Türkçe</a> <a class=\"navbar-item\" lang="
"\"uk\" hreflang=\"uk\" href=\"../uk/index.html\">Українська</a> <a class"
"=\"navbar-item\" lang=\"ur\" hreflang=\"ur\" href=\"../ur/index.html\""
">اردو</a>"
msgstr ""

#: index.html%2Bhtml.body.main.title:135-1
msgctxt "index.html+html.body.main.title:135-1"
msgid "CENO Browser | Share the Web"
msgstr ""

#: index.html%2Bhtml.body.main.title:136-1
msgctxt "index.html+html.body.main.title:136-1"
msgid "CENO Browser | Share the Web"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.img[alt]:143-9
msgid "CENO Browser banner"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.h1:148-7
msgid "Share the web!"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.p:149-3
msgid ""
"CENO (short for censorship.no!) is the world’s first mobile browser that "
"side-steps current Internet censorship methods. Its peer-to-peer backbone "
"allows people to access and share web information in and across regions "
"where connectivity has been interrupted or compromised."
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.p.a.img[alt]:151-15
msgid "Get it on Google Play"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.p.a.img[alt]:154-17
msgid "Get it on Paskoocheh"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.p.a.img[alt]:157-17
msgid "Get it on Gitlab"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.div.img[alt]:172-14
msgid "Browse Freely"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.div.h3:173-15
msgid "Unlock the Web"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.div.p:174-15
msgid ""
"Access web content through a network of cooperating peers - even when the "
"internet is down or censored. Install CENO Browser today and be prepared for "
"the next time you get 🔌 unplugged."
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.div.img[alt]:181-13
msgid "Grow the network - Become a bridge!"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.div.h3:182-15
msgid "Grow the Network"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.div.p:183-15
msgid ""
"Fight censorship by becoming a bridge! Install and run CENO Browser to "
"instantly join the network and expand the availability of blocked websites "
"to those in censored countries."
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.div.img[alt]:190-13
msgctxt "index.html+html.body.main.section.div.div.div.div.div.img[alt]:190-13"
msgid "Free and Open Source"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.div.h3:191-15
msgctxt "index.html+html.body.main.section.div.div.div.div.div.h3:191-15"
msgid "Free and Open Source"
msgstr ""

#: index.html%2Bhtml.body.main.section.div.div.div.div.div.p:192-15
msgid ""
"CENO is based on <a href=\"https://www.mozilla.org/firefox/android/\""
">Firefox for Android</a>, extended to make use of the <a href=\"https"
"://github.com/equalitie/ouinet\">Ouinet library</a> - enabling third party "
"developers to incorporate the CENO network into their apps for P2P "
"connectivity."
msgstr ""

#: index.html%2Bhtml.body.footer.div.p:213-9
msgid ""
"CENO Browser is a project by <a href=\"https://equalit.ie\">eQualit.ie</a> – "
"a not-for-profit Canadian company developing open and reusable systems with "
"a focus on privacy, online security and freedom of association. Technology "
"solutions and innovations are driven by our <a href=\"https://equalit.ie/en/"
"values/\">values</a> that guide us to protect the rights and aspirations of "
"everyone online."
msgstr ""

#.  TRANSLATORS: Please do not translate this. 
#: index.html%2Bhtml.body.footer:215-23
msgid ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document.write(\""
"-\"+new Date().getFullYear());</script>"
msgstr ""
