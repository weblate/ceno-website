msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-21 14:16-0700\n"
"PO-Revision-Date: 2023-04-21 22:59+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Russian <https://hosted.weblate.org/projects/censorship-no/"
"websitehelp/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.18-dev\n"

#: help.html%2Bhtml[lang]:4-1
msgid "en"
msgstr "ru"

#: help.html%2Bhtml.body.nav.div.div.ul.li:88-17
msgid "About"
msgstr "О проекте"

#.  TODO: html2po cannot make the href translatable, so point to the Manual's language chooser. 
#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: help.html%2Bhtml.body.nav.div.div.ul.li:91-17
msgid "Support"
msgstr "Поддержка"

#: help.html%2Bhtml.body.nav.div.div.ul.li:96-17
msgid "News"
msgstr "Новости"

#: help.html%2Bhtml.body.nav.div.div.ul.li:99-17
msgid "Community"
msgstr "Сообщество"

#: help.html%2Bhtml.body.nav.div.div.ul.li:102-17
msgid "Donate"
msgstr "дарить"

#: help.html%2Bhtml.body.nav.div.div.div.a[title]:106-15
msgid "Get CENO"
msgstr "Скачать CENO"

#: help.html%2Bhtml.body.nav.div.div.div:106-10
msgid "Download"
msgstr "Скачать"

#.  TRANSLATORS: Replace with your uppercase language code (e.g. "MY" for Burmese). 
#: help.html%2Bhtml.body.nav.div.div.div:108-1
msgid "EN"
msgstr "RU"

#.  Sorted alphabetically by language code. 
#.  TRANSLATORS: Please do not translate any of these. 
#: help.html%2Bhtml.body.nav.div.div.div.div:114-5
#, fuzzy
#| msgid ""
#| "<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/help."
#| "html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
#| "href=\"../es/help.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
#| "hreflang=\"fa\" href=\"../fa/help.html\">فارسی</a> <a class=\"navbar-"
#| "item\" lang=\"fr\" hreflang=\"fr\" href=\"../fr/help.html\">Français</a> "
#| "<a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/help."
#| "html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" "
#| "href=\"../ru/help.html\">Pусский</a> <a class=\"navbar-item\" lang=\"uk\" "
#| "hreflang=\"uk\" href=\"../uk/help.html\">Українська</a>"
msgid ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/help."
"html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
"href=\"../es/help.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
"hreflang=\"fa\" href=\"../fa/help.html\">فارسی</a> <a class=\"navbar-item\" "
"lang=\"fr\" hreflang=\"fr\" href=\"../fr/help.html\">Français</a> <a "
"class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/help."
"html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" "
"href=\"../ru/help.html\">Pусский</a> <a class=\"navbar-item\" lang=\"tr\" "
"hreflang=\"tr\" href=\"../tr/help.html\">Türkçe</a> <a class=\"navbar-item\" "
"lang=\"uk\" hreflang=\"uk\" href=\"../uk/help.html\">Українська</a> <a "
"class=\"navbar-item\" lang=\"ur\" hreflang=\"ur\" href=\"../ur/help."
"html\">اردو</a>"
msgstr ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/help."
"html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
"href=\"../es/help.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
"hreflang=\"fa\" href=\"../fa/help.html\">فارسی</a> <a class=\"navbar-item\" "
"lang=\"fr\" hreflang=\"fr\" href=\"../fr/help.html\">Français</a> <a "
"class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/help."
"html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" "
"href=\"../ru/help.html\">Pусский</a> <a class=\"navbar-item\" lang=\"uk\" "
"hreflang=\"uk\" href=\"../uk/help.html\">Українська</a>"

#: help.html%2Bhtml.body.main.title:135-1
msgctxt "help.html+html.body.main.title:135-1"
msgid "CENO Browser | Community"
msgstr "Браузер CENO | Сообщество"

#: help.html%2Bhtml.body.main.title:136-1
msgctxt "help.html+html.body.main.title:136-1"
msgid "CENO Browser | Community"
msgstr "Браузер CENO | Сообщество"

#: help.html%2Bhtml.body.main.div.h1:138-5
msgid "Get Involved!"
msgstr "Примите участие!"

#: help.html%2Bhtml.body.main.div.p:139-2
msgid ""
"We'd love your support in making CENO Browser as useful and accessible as "
"possible. CENO is a completely Free/Libre/Open Source project, and there are "
"many ways to contribute!"
msgstr ""
"Мы будем рады вашей поддержке, чтобы сделать браузер CENO максимально "
"полезным и доступным. CENO — это проект с полностью бесплатным, свободным и "
"открытым исходным кодом, внести свой вклад в который есть множество способов!"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:149-17
msgid "Translations"
msgstr "Переводы"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:151-9
msgid ""
"Help translate the CENO Browser app, web extension, user manual and website! "
"We use Weblate to crowdsource translations."
msgstr ""
"Помогите перевести приложение браузера CENO, веб-расширение, руководство "
"пользователя и веб-сайт! Для перевода мы используем краудсорсинговую "
"платформу Weblate."

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:152-13
msgid ""
"Please contact the CENO team at cenoers [at] equalit [dot] ie (or the "
"contact form below) if you're interested in becoming a reviewer for "
"translation(s) in your language(s)."
msgstr ""
"Пожалуйста, свяжитесь с командой CENO на cenoers [at] equalit [dot] ie ( или "
"через контактную форму ниже), если вы хотели бы проверять перевод(ы) на "
"вашем языке(ах)."

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:153-13
msgid ""
"For more information about how to use Weblate, see <a href=\"https://docs."
"weblate.org/\">the Weblate documentation.</a>"
msgstr ""
"Дополнительную информацию об использовании Weblate можно получить в <a "
"href=\"https://docs.weblate.org/\">документации Weblate.</a>"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:154-17
msgid "Join our Weblate"
msgstr "Присоединяйтесь к Weblate"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:165-17
msgid "User Research"
msgstr "Исследование пользователей"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:166-3
msgid ""
"CENO relies on user feedback to understand the needs of our users and to "
"suggest improvements and new features."
msgstr ""
"CENO полагается на отзывы пользователей, чтобы понять потребности наших "
"пользователей и предложить улучшения и новые функции."

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:167-3
msgid ""
"Sharing your story also helps spread the word about CENO, which grows the "
"network. With your consent, we may also highlight your story on our website!"
msgstr ""
"Делясь своей историей, вы также помогаете распространять информацию о CENO, "
"что способствует расширению сети. С вашего согласия мы также можем "
"разместить вашу историю на нашем веб-сайте!"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:168-3
msgid ""
"We welcome reviews on the Play Store as well as direct communications - you "
"can get in touch with the team via the feedback button at the bottom of this "
"website, or send us an email:"
msgstr ""
"Мы приветствуем отзывы в Play Store, а также прямые сообщения - вы можете "
"связаться с командой через кнопку обратной связи внизу этого веб-сайта или "
"отправить нам электронное письмо:"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:169-3
msgid "cenoers [at] equalit [dot] ie"
msgstr "cenoers [at] equalit [dot] ie"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:170-17
msgid "Take a survey"
msgstr "Пройти опрос"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:181-17
msgid "Source Code"
msgstr "Исходный код"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:182-3
msgid ""
"If you are interested in CENO's source code please check the following Git "
"repositories:"
msgstr ""
"Если вас интересует исходный код, ознакомьтесь со следующими репозиториями "
"Git:"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p.ul.li:184-12
msgid ""
"Browser components: <a href=\"https://github.com/censorship-no/ceno-"
"browser\">package builder</a>, <a href=\"https://github.com/censorship-no/"
"gecko-dev/tree/ceno\">Firefox fork</a>, <a href=\"https:// ithub.com/"
"censorship-no/ceno-distribution\">included extensions</a>, <a href=\"https://"
"github.com/censorship-no/ceno-ext-settings\">settings extension source</a>"
msgstr ""
"Компоненты Браузера: <a href=\"https://github.com/censorship-no/ceno-"
"browser\">компоновщик пакетов</a>, <a href=\"https://github.com/censorship-"
"no/gecko-dev/tree/ceno\">форк Firefox</a>, <a href=\"https://github.com/"
"censorship-no/ceno-distribution\">включенные расширения</a>, <a "
"href=\"https://github.com/censorship-no/ceno-ext-settings\">исходный код "
"расширения настроек</a>"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p.ul.li:186-12
msgid "Ouinet source"
msgstr "Исходный код Ouinet"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.p:187-12
msgid ""
"You may also be interested in the (no longer maintained) <a href=\"https://"
"github.com/censorship-no-archive/ceno1\">previous incarnation of CENO</a>, "
"built on the <a href=\"https://freenetproject.org/\">Freenet</a> anonymous "
"file sharing and content publishing network. Other inactive project-related "
"repositories can be found at the <a href=\"https://github.com/censorship-no-"
"archive\">Censorship.no! archive</a>."
msgstr ""
"Вас также может заинтересовать (больше не поддерживаемая) <a href=\"https://"
"github.com/censorship-no-archive/ceno1\">предыдущая версия CENO</a>, "
"построенная на сети анонимного обмена файлами и публикации контента <a "
"href=\"https://freenetproject.org/\">Freenet</a>. Другие неактивные "
"репозитории проекта можно найти в <a href=\"https://github.com/censorship-no-"
"archive\">архиве Censorship.no!</a>."

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:188-17
msgid "Browse our Github"
msgstr "Посмотрите наш Github"

#: help.html%2Bhtml.body.main.div.section.h2:201-1
msgid "Run a Bridge Node"
msgstr "Поддерживать мост"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:207-3
msgid "Become a bridge!"
msgstr "Станьте мостом!"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:209-17
msgid "Want to help grow the network? Join the swarm!"
msgstr "Хотите помочь с расширением сети? Присоединяйтесь к нам!"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:216-3
msgid "The easy way: Android"
msgstr "Максимально просто: Android"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:218-17
msgid ""
"Run CENO Browser on your Android device, and/or a spare, always-on device."
msgstr ""
"Запустите Браузер CENO на личном (или другом постоянно включенном) "
"устройстве Android."

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:225-3
msgid "Run the CENO Docker Client"
msgstr "Запустите клиент CENO в Docker"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:227-17
msgid ""
"Alternatively, with just one command you can set up a bridge on your "
"computer or VPS: <code>sudo docker run --name ceno-client -dv ceno:/var/opt/"
"ouinet --network host --restart unless-stopped equalitie/ceno-client</code>"
msgstr ""
"Кроме того, с помощью всего одной команды вы можете настроить мост на своем "
"компьютере или VPS: <code>sudo docker run --name ceno-client -dv ceno:/var/"
"opt/ouinet --network host --restart unless-stopped equalitie/ceno-client</"
"code>"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:234-3
msgid "Open a portal to the internet"
msgstr "ОТКРОЙТЕ ДОСТУП В ИНТЕРНЕТ"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:236-17
msgid "Ensure UPnP is enabled on your router, or set up port forwarding."
msgstr ""
"Убедитесь, что на вашем маршрутизаторе включен UPnP, или настройте "
"переадресацию портов."

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:243-3
msgid "To see if it's working:"
msgstr "ЧТОБЫ ПРОВЕРИТЬ, ВСЁ ЛИ РАБОТАЕТ:"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:245-17
msgid ""
"Create and open a new Firefox Profile, and set the proxy to localhost, port "
"8077."
msgstr ""
"Создайте и новый профиль Firefox и настройте в качестве прокси-сервера "
"localhost на порту 8077."

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:252-3
msgid "Thank you for growing the network!"
msgstr "СПАСИБО, ЧТО ВЫ С НАМИ!"

#: help.html%2Bhtml.body.main.div.section.div.div.div.div.div.p:254-17
msgid ""
"Install the CENO Web Extension and the provided certificate to test it out!"
msgstr ""
"Установите веб-расширение CENO и предоставленный сертификат, чтобы всё "
"протестировать!"

#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: help.html%2Bhtml.body.main.div.section.div.div:260-1
msgid "Learn more about setting up a bridge in the User Manual!"
msgstr ""
"Более подробную информацию о настройке моста можно получить в руководстве "
"пользователя!"

#: help.html%2Bhtml.body.footer.div.p:281-9
msgid ""
"CENO Browser is a project by <a href=\"https://equalit.ie\">eQualit.ie</a> – "
"a not-for-profit Canadian company developing open and reusable systems with "
"a focus on privacy, online security and freedom of association. Technology "
"solutions and innovations are driven by our <a href=\"https://equalit.ie/en/"
"values/\">values</a> that guide us to protect the rights and aspirations of "
"everyone online."
msgstr ""
"CENO Browser - это проект, разработанный <a href=\"https://equalit."
"ie\">eQualit.ie</a>- некоммерческая канадская организация, разрабатывающая "
"открытые и многоразовые системы с акцентом на конфиденциальность, онлайн-"
"безопасность и свободу убеждений. В основе технологических решений и "
"инноваций лежат наши<a href=\"https://equalit.ie/en/values/\">ценности</a>, "
"которые определяют наши действия по защите прав и стремлений каждого "
"человека в Интернете."

#.  TRANSLATORS: Please do not translate this. 
#: help.html%2Bhtml.body.footer:283-23
msgid ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document."
"write(\"-\"+new Date().getFullYear());</script>"
msgstr ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document."
"write(\"-\"+new Date().getFullYear());</script>"

#~ msgctxt "help.html+html.body.main.title:132-1"
#~ msgid "CENO Browser | Community"
#~ msgstr "Браузер CENO | Сообщество"

#~ msgctxt "help.html+html.body.main.title:129-1"
#~ msgid "CENO Browser | Community"
#~ msgstr "Браузер CENO | Сообщество"

#~ msgctxt "help.html+html.body.main.title:128-1"
#~ msgid "CENO Browser | Community"
#~ msgstr "Браузер CENO | Сообщество"

#~ msgctxt "help.html+html.body.main.title:127-1"
#~ msgid "CENO Browser | Community"
#~ msgstr "Браузер CENO | Сообщество"

#~ msgid ""
#~ "<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/help."
#~ "html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
#~ "href=\"../es/help.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
#~ "hreflang=\"fa\" href=\"../fa/help.html\">فارسی</a> <a class=\"navbar-"
#~ "item\" lang=\"my\" hreflang=\"my\" href=\"../my/help.html\">မြန်မာစာ</a> "
#~ "<a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../ru/help."
#~ "html\">Pусский</a> <a class=\"navbar-item\" lang=\"uk\" hreflang=\"uk\" "
#~ "href=\"../uk/help.html\">Українська</a>"
#~ msgstr ""
#~ "<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/help."
#~ "html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
#~ "href=\"../es/help.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
#~ "hreflang=\"fa\" href=\"../fa/help.html\">فارسی</a> <a class=\"navbar-"
#~ "item\" lang=\"my\" hreflang=\"my\" href=\"../my/help.html\">မြန်မာစာ</a> "
#~ "<a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../ru/help."
#~ "html\">Pусский</a> <a class=\"navbar-item\" lang=\"uk\" hreflang=\"uk\" "
#~ "href=\"../uk/help.html\">Українська</a>"

#~ msgid ""
#~ "﻿Run the CENO Docker client: ﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿Alternatively, with just one command you can "
#~ "set up a bridge on your computer or VPS."
#~ msgstr ""
#~ "Запустите клиент CENO в Docker: Кроме того, с помощью всего одной команды "
#~ "вы можете настроить мост на своем компьютере или VPS."

#~ msgid ""
#~ "To learn more about setting up a bridge, please consult the <a href=\"../"
#~ "user-manual/en/browser/bridging.html\">user manual.</a>"
#~ msgstr ""
#~ "Более подробную информацию о настройке моста можно получить в <a "
#~ "href=\"../user-manual/ru/browser/bridging.html\">руководстве пользователя."
#~ "</a>"

#~ msgid "To get started:"
#~ msgstr "Для начала:"

#~ msgid ""
#~ "Make an account on <a href=\"https://weblate.org/\">Weblate</a> or sign "
#~ "in with your Github or Gitlab ID. Note: Account creation is not required, "
#~ "but you will only be able to suggest translations in this case."
#~ msgstr ""
#~ "Зарегистрируйтесь на сайте <a href=\"https://weblate.org/\">Weblate</a> "
#~ "или войдите через свой аккаунт на Github или Gitlab. Примечание: Создание "
#~ "учетной записи не требуется, но в этом случае вы сможете только "
#~ "предлагать переводы."

#~ msgid ""
#~ "Visit the <a href=\"https://hosted.weblate.org/projects/censorship-no/"
#~ "\">Censorship.no project page</a> - which lists all components and their "
#~ "strings (including Android app strings, the web browser extension, the "
#~ "User Manual and the website)."
#~ msgstr ""
#~ "Посетите <a href=\"https://hosted.weblate.org/projects/censorship-no/"
#~ "\">страницу проекта Censorship.no</a>, где перечислены все компоненты "
#~ "проекта и их строки (включая строки приложения Android, расширения веб-"
#~ "браузера, руководства пользователя и веб-сайта)."

#~ msgid ""
#~ "On the <a href=\"https://hosted.weblate.org/projects/censorship-no/"
#~ "#languages\">Censorship.no project page's 'Languages' tab</a>, choose the "
#~ "language you want to work on, or -- in case the language doesn't exist "
#~ "yet -- initiate a new language by selecting a component from the main "
#~ "project page and clicking on the \"Start a new translation\" button."
#~ msgstr ""
#~ "Во вкладке <a href=\"https://hosted.weblate.org/projects/censorship-no/"
#~ "#languages\">«Языки» на странице проекта Censorship.no</a> выберите язык, "
#~ "над которым хотите поработать, или — если не найдете свой язык — добавьте "
#~ "новый язык, выбрав компонент на главной странице проекта и нажав кнопку "
#~ "«Начать новый перевод»."

#~ msgid "Censorship.no! - Get Involved!"
#~ msgstr "Censorship.no! — Примите участие!"

#~ msgid "How It Works"
#~ msgstr "Как это работает"

#~ msgid "User Manual"
#~ msgstr "Руководство пользователя"

#~ msgctxt "help.html+html.body.main.div.h1:123-5"
#~ msgid "Get Involved!"
#~ msgstr "Примите участие!"

#~ msgctxt "help.html+html.body.main.div.h1:125-5"
#~ msgid "Get Involved!"
#~ msgstr "Примите участие!"

#~ msgctxt "help.html+html.body.nav.div.div.ul.li:93-17"
#~ msgid "Get Involved!"
#~ msgstr "Примите участие!"

#~ msgctxt "help.html+html.body.main.div.h1:121-5"
#~ msgid "Get Involved!"
#~ msgstr "Примите участие!"
