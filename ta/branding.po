msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-21 14:16-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 3.3.2\n"

#: branding.html%2Bhtml[lang]:4-1
msgid "en"
msgstr ""

#: branding.html%2Bhtml.body.nav.div.div.ul.li:88-17
msgid "About"
msgstr ""

#.  TODO: html2po cannot make the href translatable, so point to the Manual's language chooser. 
#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: branding.html%2Bhtml.body.nav.div.div.ul.li:91-17
msgid "Support"
msgstr ""

#: branding.html%2Bhtml.body.nav.div.div.ul.li:96-17
msgid "News"
msgstr ""

#: branding.html%2Bhtml.body.nav.div.div.ul.li:99-17
msgid "Community"
msgstr ""

#: branding.html%2Bhtml.body.nav.div.div.ul.li:102-17
msgid "Donate"
msgstr ""

#: branding.html%2Bhtml.body.nav.div.div.div.a[title]:106-15
msgid "Get CENO"
msgstr ""

#: branding.html%2Bhtml.body.nav.div.div.div:106-10
msgid "Download"
msgstr ""

#.  TRANSLATORS: Replace with your uppercase language code (e.g. "MY" for Burmese). 
#: branding.html%2Bhtml.body.nav.div.div.div:108-1
msgid "EN"
msgstr ""

#.  Sorted alphabetically by language code. 
#.  TRANSLATORS: Please do not translate any of these. 
#: branding.html%2Bhtml.body.nav.div.div.div.div:114-5
msgid ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/branding."
"html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
"href=\"../es/branding.html\">Español</a> <a class=\"navbar-item\" "
"lang=\"fa\" hreflang=\"fa\" href=\"../fa/branding.html\">فارسی</a> <a "
"class=\"navbar-item\" lang=\"fr\" hreflang=\"fr\" href=\"../fr/branding."
"html\">Français</a> <a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" "
"href=\"../my/branding.html\">မြန်မာစာ</a> <a class=\"navbar-item\" "
"lang=\"ru\" hreflang=\"ru\" href=\"../ru/branding.html\">Pусский</a> <a "
"class=\"navbar-item\" lang=\"tr\" hreflang=\"tr\" href=\"../tr/branding."
"html\">Türkçe</a> <a class=\"navbar-item\" lang=\"uk\" hreflang=\"uk\" "
"href=\"../uk/branding.html\">Українська</a> <a class=\"navbar-item\" "
"lang=\"ur\" hreflang=\"ur\" href=\"../ur/branding.html\">اردو</a>"
msgstr ""

#: branding.html%2Bhtml.body.main.title:135-1
msgctxt "branding.html+html.body.main.title:135-1"
msgid "CENO Browser | Brand and Style Guide"
msgstr ""

#: branding.html%2Bhtml.body.main.title:136-1
msgctxt "branding.html+html.body.main.title:136-1"
msgid "CENO Browser | Brand and Style Guide"
msgstr ""

#: branding.html%2Bhtml.body.main.div.h1:138-5
msgid "Brand and Style Guide"
msgstr ""

#: branding.html%2Bhtml.body.main.div.h2:140-2
msgctxt "branding.html+html.body.main.div.h2:140-2"
msgid "Logo"
msgstr ""

#: branding.html%2Bhtml.body.main.div.p:141-2
msgid ""
"In order to preserve accessibility, the color version must be used in white "
"backgrounds and the opposite."
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.h3:143-26
msgctxt "branding.html+html.body.main.div.div.div.h3:143-26"
msgid "Logo"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.h3:147-26
msgid "Logo Icon"
msgstr ""

#: branding.html%2Bhtml.body.main.div.h2:153-2
msgid "Colors"
msgstr ""

#: branding.html%2Bhtml.body.main.div.p:154-9
msgid "These are the colors that represent Ceno’s brand identity."
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.h3:158-33
msgid "Primary Color"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.div.h3:161-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.div.h3:161-15"
msgid "Light Blue"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.div.p:162-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.div.p:162-15"
msgid "#0EA5E9"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.h3:166-33
msgid "Secondary Color"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.div.h3:169-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.div.h3:169-15"
msgid "Orange"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.div.p:170-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.div.p:170-15"
msgid "#F97316"
msgstr ""

#: branding.html%2Bhtml.body.main.div.h2:178-2
msgid "UI Colors"
msgstr ""

#: branding.html%2Bhtml.body.main.div.p:179-9
msgid ""
"For accessibility and readability reasons, here’s the extended Color Palette "
"of 10 different shades of both brand colors. When colors are used in text, "
"according to Web Content Accessibility Guidelines (WCAG) 2, Level AA "
"requires the contrast ratio to be at least 4.5:1. Any color of the palette "
"can be used if this requirement is met."
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.h3:186-11
msgctxt "branding.html+html.body.main.div.div.h3:186-11"
msgid "Light Blue"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:190-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:190-15"
msgid "50"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:191-8
msgid "#F0F9FF"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:196-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:196-15"
msgid "500"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:197-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:197-15"
msgid "#0EA5E9"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:204-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:204-15"
msgid "100"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:205-15
msgid "#E0F2FE"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:210-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:210-15"
msgid "600"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:211-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:211-15"
msgid "#0284C7"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:219-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:219-15"
msgid "200"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:220-15
msgid "#BAE6FD"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:225-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:225-15"
msgid "700"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:226-15
msgid "#0369A1"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:234-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:234-15"
msgid "300"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:235-15
msgid "#7DD3FC"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:240-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:240-15"
msgid "800"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:241-15
msgid "#075985"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:249-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:249-15"
msgid "400"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:250-15
msgid "#38BDF8"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:255-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:255-15"
msgid "900"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:256-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:256-15"
msgid "#0284C7"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.h3:262-11
msgctxt "branding.html+html.body.main.div.div.h3:262-11"
msgid "Orange"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:266-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:266-15"
msgid "50"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:267-15
msgid "#FFF7ED"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:272-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:272-15"
msgid "500"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:273-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:273-15"
msgid "#F97316"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:280-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:280-15"
msgid "100"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:281-15
msgid "#FFEDD5"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:286-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:286-15"
msgid "600"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:287-15
msgid "#EA580C"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:295-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:295-15"
msgid "200"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:296-15
msgid "#FED7AA"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:301-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:301-15"
msgid "700"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:302-15
msgid "#C2410C"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:309-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:309-15"
msgid "300"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:310-15
msgid "#FDBA74"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:315-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:315-15"
msgid "800"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:316-15
msgid "#9A3412"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:324-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:324-15"
msgid "400"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:325-15
msgid "#FB923C"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:330-15
msgctxt "branding.html+html.body.main.div.div.div.div.div.p:330-15"
msgid "900"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.div.p:331-15
msgid "#7C2D12"
msgstr ""

#: branding.html%2Bhtml.body.main.div.h2:338-1
msgid "Typography"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.p:341-9
msgid "Primary"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.p:342-9
msgid ""
"<strong>Metropolis</strong> is used as primary font of the visual identity. "
"It can be used in lower and upper cases. It comes with a family of styles. "
"This font is used in texts such as titles and headers."
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.p:348-9
msgid "Secondary"
msgstr ""

#: branding.html%2Bhtml.body.main.div.div.div.div.p:349-9
msgid ""
"<strong>Inter</strong> is used as secondary font of the visual identity. It "
"can be used in lower and upper cases. It comes with a family of styles. This "
"font is used in texts such as body texts."
msgstr ""

#: branding.html%2Bhtml.body.footer.div.p:371-9
msgid ""
"CENO Browser is a project by <a href=\"https://equalit.ie\">eQualit.ie</a> – "
"a not-for-profit Canadian company developing open and reusable systems with "
"a focus on privacy, online security and freedom of association. Technology "
"solutions and innovations are driven by our <a href=\"https://equalit.ie/en/"
"values/\">values</a> that guide us to protect the rights and aspirations of "
"everyone online."
msgstr ""

#.  TRANSLATORS: Please do not translate this. 
#: branding.html%2Bhtml.body.footer:373-23
msgid ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document."
"write(\"-\"+new Date().getFullYear());</script>"
msgstr ""
