msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-21 14:16-0700\n"
"PO-Revision-Date: 2023-04-23 13:48+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Urdu <https://hosted.weblate.org/projects/censorship-no/"
"indexhtml/ur/>\n"
"Language: ur\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.18-dev\n"

#: about.html%2Bhtml[lang]:4-1
msgid "en"
msgstr "en"

#: about.html%2Bhtml.body.nav.div.div.ul.li:88-17
msgid "About"
msgstr "کے بارے میں"

#.  TODO: html2po cannot make the href translatable, so point to the Manual's language chooser. 
#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: about.html%2Bhtml.body.nav.div.div.ul.li:91-17
msgid "Support"
msgstr "معاونت"

#: about.html%2Bhtml.body.nav.div.div.ul.li:96-17
msgid "News"
msgstr "خبریں"

#: about.html%2Bhtml.body.nav.div.div.ul.li:99-17
msgid "Community"
msgstr "کمیونٹی"

#: about.html%2Bhtml.body.nav.div.div.ul.li:102-17
msgid "Donate"
msgstr "عطیہ کریں"

#: about.html%2Bhtml.body.nav.div.div.div.a[title]:106-15
msgid "Get CENO"
msgstr "CENO حاصل کریں"

#: about.html%2Bhtml.body.nav.div.div.div:106-10
msgid "Download"
msgstr "ڈاؤن لوڈ کریں"

#.  TRANSLATORS: Replace with your uppercase language code (e.g. "MY" for Burmese). 
#: about.html%2Bhtml.body.nav.div.div.div:108-1
msgid "EN"
msgstr "UR"

#.  Sorted alphabetically by language code. 
#.  TRANSLATORS: Please do not translate any of these. 
#: about.html%2Bhtml.body.nav.div.div.div.div:114-5
msgid ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/about."
"html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
"href=\"../es/about.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
"hreflang=\"fa\" href=\"../fa/about.html\">فارسی</a> <a class=\"navbar-item\" "
"lang=\"fr\" hreflang=\"fr\" href=\"../fr/about.html\">Français</a> <a "
"class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/about."
"html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" "
"href=\"../ru/about.html\">Pусский</a> <a class=\"navbar-item\" lang=\"tr\" "
"hreflang=\"tr\" href=\"../tr/about.html\">Türkçe</a> <a class=\"navbar-"
"item\" lang=\"uk\" hreflang=\"uk\" href=\"../uk/about.html\">Українська</a> "
"<a class=\"navbar-item\" lang=\"ur\" hreflang=\"ur\" href=\"../ur/about."
"html\">اردو</a>"
msgstr ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/about."
"html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" href="
"\"../es/about.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
"hreflang=\"fa\" href=\"../fa/about.html\">فارسی</a> <a class=\"navbar-item\" "
"lang=\"fr\" hreflang=\"fr\" href=\"../fr/about.html\">Français</a> <a class"
"=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/about.html\""
">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../"
"ru/about.html\">Pусский</a> <a class=\"navbar-item\" lang=\"tr\" hreflang="
"\"tr\" href=\"../tr/about.html\">Türkçe</a> <a class=\"navbar-item\" lang="
"\"uk\" hreflang=\"uk\" href=\"../uk/about.html\">Українська</a> <a class"
"=\"navbar-item\" lang=\"ur\" hreflang=\"ur\" href=\"../ur/about.html\""
">اردو</a>"

#: about.html%2Bhtml.body.main.title:135-1
msgctxt "about.html+html.body.main.title:135-1"
msgid "CENO Browser | About"
msgstr "CENO براؤزر | ہمارے متعلق"

#: about.html%2Bhtml.body.main.title:136-1
msgctxt "about.html+html.body.main.title:136-1"
msgid "CENO Browser | About"
msgstr "CENO براؤزر | ہمارے متعلق"

#: about.html%2Bhtml.body.main.div.h1:139-6
msgid "How CENO Browser Works"
msgstr "CENO براؤزر کیسے کام کرتا ہے"

#: about.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:147-3
msgid ""
"Most web traffic in countries that filter internet is routed through "
"centralized exchanges, such as Google."
msgstr ""
"انٹرنیٹ کو فلٹر کرنے والے ممالک میں زیادہ تر ویب ٹریفک مرکزی تبادلوں کے "
"ذریعہ روٹ کیا جاتا ہے ، جیسے گوگل۔"

#: about.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:155-3
msgid ""
"This design gives the censor an advantage in locating and blocking popular "
"proxies, VPN servers, relays, etc;"
msgstr ""
"یہ ڈیزائن سنسر کو مقبول پراکسیز، وی پی این سرورز، ریلے وغیرہ; کو تلاش کرنے "
"اور بلاک کرنے میں فائدہ دیتا ہے"

#: about.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:163-3
msgid ""
"In these cases, reaching origin web servers is difficult or impossible, and "
"your device will not be able to get that content."
msgstr ""
"ان صورتوں میں، اصل ویب سرورز تک پہنچنا مشکل یا ناممکن ہے، اور آپ کا ڈیوائس "
"وہ مواد حاصل نہیں کر سکے گا۔"

#: about.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:171-3
msgid ""
"CENO is a mobile web browser for storing and sharing web content anywhere."
msgstr ""
"CENO ایک موبائل ویب براؤزر ہے جو ویب مواد کو کہیں بھی اسٹور کرنے اور اشتراک "
"کرنے کے لیے ہے۔"

#: about.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:179-3
msgid ""
"CENO users create small-world networks to connect to each other, and "
"thereafter to a bridge outside the censored zone."
msgstr ""
"CENO صارفین ایک دوسرے سے جڑنے کے لیے چھوٹے دنیا کے نیٹ ورک بناتے ہیں، اور اس "
"کے بعد سنسر شدہ زون سے باہر ایک پل تک۔"

#: about.html%2Bhtml.body.main.div.section.div.div.div.div.div.h4:187-3
msgid ""
"Once a website is accessed by a single CENO user, it is stored and shared "
"inside the country peer-to-peer."
msgstr ""
"ایک بار جب کسی ایک CENO صارف کے ذریعہ کسی ویب سائٹ تک رسائی حاصل کی جاتی ہے، "
"تو اسے ملک کے اندر ذخیرہ اور اشتراک کیا جاتا ہے۔"

#.  TRANSLATORS: Replace "en" in the link by your language code if the Manual is available for it. 
#: about.html%2Bhtml.body.main.div.div:195-1
msgid ""
"<a href=\"../user-manual/en/\" style=\"text-align:center\"><button "
"class=\"btn btn-outline-danger btn-learnmore mt-3 mb-3\" type=\"button\" "
"style=\"text-align: center;\"><span class=\"fas fa-book\"></span> Learn more "
"in the User Manual!</button></a> <a href=\"https://github.com/equalitie/"
"ouinet/blob/master/doc/ouinet-network-whitepaper.md\"><button class=\"btn "
"btn-outline-danger btn-learnmore\" type=\"button\"><span class=\"fas fa-"
"scroll\"></span> For a more in-depth, technical guide, see the Ouinet "
"whitepaper.</button></a>"
msgstr ""
"<a href=\"../user-manual/en/\" style=\"text-align:center\"><button "
"class=\"btn btn-outline-danger btn-learnmore mt-3 mb-3\" type=\"button\" "
"style=\"text-align: center;\"><span class=\"fas fa-book\"></span> یوزر "
"مینوئل میں مزید جانیں!</button></a> <a href=\"https://github.com/equalitie/"
"ouinet/blob/master/doc/ouinet-network-whitepaper.md\"><button class=\"btn "
"btn-outline-danger btn-learnmore\" type=\"button\"><span class=\"fas fa-"
"scroll\"></span> مزید گہرائی سے، تکنیکی گائیڈ کے لیے، Ouinet وائٹ پیپر "
"دیکھیں۔</button></a>"

#: about.html%2Bhtml.body.footer.div.p:215-9
msgid ""
"CENO Browser is a project by <a href=\"https://equalit.ie\">eQualit.ie</a> – "
"a not-for-profit Canadian company developing open and reusable systems with "
"a focus on privacy, online security and freedom of association. Technology "
"solutions and innovations are driven by our <a href=\"https://equalit.ie/en/"
"values/\">values</a> that guide us to protect the rights and aspirations of "
"everyone online."
msgstr ""
"CENO براؤزر <a href=\"https://equalit.ie\">eQualit.ie</a>کا ایک پروجیکٹ ہے – "
"ایک غیر منافع بخش کینیڈا کی کمپنی جو رازداری، آن لائن سیکیورٹی اور ایسوسی "
"ایشن کی آزادی پر توجہ کے ساتھ کھلے اور دوبارہ استعمال کے قابل نظام تیار کرتی "
"ہے۔ ٹیکنالوجی کے حل اور اختراعات ہماری <a href=\"https://equalit.ie/en/"
"values/\">اقدار</a> سے چلتی ہیں جو آن لائن ہر کسی کے حقوق اور خواہشات کے "
"تحفظ کے لیے ہماری رہنمائی کرتی ہیں۔"

#.  TRANSLATORS: Please do not translate this. 
#: about.html%2Bhtml.body.footer:217-23
msgid ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document."
"write(\"-\"+new Date().getFullYear());</script>"
msgstr ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document."
"write(\"-\"+new Date().getFullYear());</script>"

#~ msgctxt "about.html+html.body.main.title:132-1"
#~ msgid "CENO Browser | About"
#~ msgstr "CENO براؤزر | ہمارے متعلق"
